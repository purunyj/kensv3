/*
 * E_TCPAssignment.cpp
 *
 *  Created on: 2014. 11. 20.
 *      Author: 근홍
 */


#include <E/E_Common.hpp>
#include <E/Networking/E_Host.hpp>
#include <E/Networking/E_Networking.hpp>
#include <cerrno>
#include <E/Networking/E_Packet.hpp>
#include <E/Networking/E_NetworkUtil.hpp>
#include "TCPAssignment.hpp"
#include <map>
#include <list>

namespace E
{

	TCPAssignment::TCPAssignment(Host* host) : HostModule("TCP", host),
			NetworkModule(this->getHostModuleName(), host->getNetworkSystem()),
			SystemCallInterface(AF_INET, IPPROTO_TCP, host),
			NetworkLog(host->getNetworkSystem()),
			TimerModule(host->getSystem())
	{

	}

	TCPAssignment::~TCPAssignment()
	{

	}

	void TCPAssignment::initialize()
	{
		
	}

	void TCPAssignment::finalize()
	{

	}

	void TCPAssignment::systemCallback(UUID syscallUUID, int pid, const SystemCallParameter& param)
	{
		switch(param.syscallNumber)
		{
		case SOCKET:
			this->syscall_socket(syscallUUID, pid, param.param1_int, param.param2_int);
			break;
		case CLOSE:
			this->syscall_close(syscallUUID, pid, param.param1_int);
			break;
		case READ:
			this->syscall_read(syscallUUID, pid, param.param1_int, param.param2_ptr, param.param3_int);
			break;
		case WRITE:
			this->syscall_write(syscallUUID, pid, param.param1_int, param.param2_ptr, param.param3_int);
			break;
		case CONNECT:
			this->syscall_connect(syscallUUID, pid, param.param1_int,
					static_cast<struct sockaddr*>(param.param2_ptr), (socklen_t)param.param3_int);
			break;
		case LISTEN:
			this->syscall_listen(syscallUUID, pid, param.param1_int, param.param2_int);
			break;
		case ACCEPT:
			this->syscall_accept(syscallUUID, pid, param.param1_int,
					static_cast<struct sockaddr*>(param.param2_ptr),
					static_cast<socklen_t*>(param.param3_ptr));
			break;
		case BIND:
			this->syscall_bind(syscallUUID, pid, param.param1_int,
					static_cast<struct sockaddr *>(param.param2_ptr),
					(socklen_t) param.param3_int);
			break;
		case GETSOCKNAME:
			this->syscall_getsockname(syscallUUID, pid, param.param1_int,
					static_cast<struct sockaddr *>(param.param2_ptr),
					static_cast<socklen_t*>(param.param3_ptr));
			break;
		case GETPEERNAME:
			this->syscall_getpeername(syscallUUID, pid, param.param1_int,
					static_cast<struct sockaddr *>(param.param2_ptr),
					static_cast<socklen_t*>(param.param3_ptr));
			break;
		default:
			assert(0);
		}
	}

	void TCPAssignment::packetArrived(std::string fromModule, Packet* packet)
	{
		uint8_t src_ip[4];
		uint8_t dest_ip[4];

		uint16_t data_size = packet->getSize() - 54;

		struct tcpdata rcvdata;
		packet->readData(14+12, src_ip, 4);
		packet->readData(14+16, dest_ip, 4);
		packet->readData(34, &rcvdata, 20 + data_size);

		uint16_t have_good_checksum = NetworkUtil::tcp_sum(*(uint32_t *)src_ip,*(uint32_t *)dest_ip, (uint8_t*)&rcvdata,  20 + data_size);

		if(!(have_good_checksum == 0 || have_good_checksum == 0xffff)){ // checksum fail
			this->freePacket(packet);
			return;
		}

		int pid, sockfd;
		//printf("arrived\n");
		//printf("%x %x\n",*(uint32_t *)src_ip, *(uint32_t *)dest_ip);
		//printf("%d %d\n",src_port, dest_port);

		//search all listen with dest match or 1 connection dest, src match
		struct context *dest_context;
		bool exist_dest_context = false;
		bool exist_connection = false;
		bool is_inaddr = true;
		std::map<int, std::map<int, struct context>>::iterator iter;
		std::map<int, struct context>::iterator iter2;
		for (iter = pid_map.begin(); iter != pid_map.end() && !exist_connection; ++iter) {
			for (iter2 = (iter->second.begin()); iter2 != iter->second.end() && !exist_connection; ++iter2){
				//struct sockaddr_in *peer_info = (struct sockaddr_in *)(&(iter2->second.peer_addr));
				struct sockaddr_in *my_info = (struct sockaddr_in *)(&(iter2->second.my_addr));
				//printf("%x %x\n",peer_info->sin_addr.s_addr, my_info->sin_addr.s_addr);
				//printf("%d %d\n",peer_info->sin_port, my_info->sin_port);
				switch(iter2->second.state){
				case state::LISTEN:
					//printf("something\n");
					if(is_inaddr && my_info->sin_port == rcvdata.dest_port && ((((in_addr *)dest_ip)->s_addr == my_info->sin_addr.s_addr) || my_info->sin_addr.s_addr == INADDR_ANY)){
						pid = iter->first;
						sockfd = iter2->first;
						dest_context = &iter2->second;
						exist_dest_context = true;
						if(my_info->sin_addr.s_addr != INADDR_ANY){
							is_inaddr = false;
						}
					}
					break;
				case state::CLOSED:
					break;
				default:
					struct sockaddr_in *peer_info = (struct sockaddr_in *)(&(iter2->second.peer_addr));
					if(my_info->sin_port == rcvdata.dest_port && (((in_addr *)dest_ip)->s_addr == my_info->sin_addr.s_addr) &&peer_info->sin_port == rcvdata.src_port && ((in_addr *)src_ip)->s_addr == peer_info->sin_addr.s_addr){
						pid = iter->first;
						sockfd = iter2->first;
						dest_context = &iter2->second;
						iter2 = iter->second.end();
						iter = pid_map.end();
						exist_dest_context = true;
						exist_connection = true;
					}
				}
			}
		}

		if(exist_dest_context){
			switch(dest_context->state){
			case state::LISTEN:
			{
				std::list<struct context>::iterator iter3;
				struct sockaddr_in *peer_info;
				struct context *listening;
				//already connect SYN_RCVD
				for (iter3 = dest_context->connect_context.begin(); iter3 != dest_context->connect_context.end(); ++iter3) {
					switch(iter3->state){
					case state::SYN_RCVD:
					{
						peer_info = (struct sockaddr_in *)(&(iter3->peer_addr));
						if(peer_info->sin_port == rcvdata.src_port && *((uint32_t *)src_ip) == peer_info->sin_addr.s_addr){
							if(rcvdata.flag2 == flag2::ACK){
								//printf("esta!!!!!\n");
								if(iter3->seqnum != rcvdata.acknum){
									this->freePacket(packet);
									return;
								}

								dest_context->esta += 1;
								dest_context->connecting -= 1;

								iter3->state = state::ESTABLISHED;
								iter3->buf = (uint8_t *)malloc(ntohs(dest_context->winsize));
								iter3->peer_winsize = rcvdata.winsize;
								//iter3->first_pos = iter3->acknum;
								
								iter3->recentpacket.pop_time = 0;
								cancelTimer(iter3->recentpacket.syscallUUID);
								this->freePacket(iter3->recentpacket.this_packet);

								if(iter3->accept){
									//printf("accept complete\n");
									if((sockfd = SystemCallInterface::createFileDescriptor(pid))<0){
										this->freePacket(packet);
										return;
									}
									dest_context->esta -= 1;
									
									*iter3->acceptaddr = iter3->peer_addr; // update accpet return addrinfo

									pid_map[pid][sockfd]=*iter3;
									using_ports.push_back(((struct sockaddr_in*)(&pid_map[pid][sockfd].my_addr))->sin_port);
									dest_context->connect_context.erase(iter3);
									returnSystemCall(pid_map[pid][sockfd].syscallUUID, sockfd);
								}
								this->freePacket(packet);
								return;
							}
							else if(rcvdata.flag2 == flag2::SYN){
								iter3->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
								cancelTimer(iter3->recentpacket.syscallUUID);
								iter3->recentpacket.syscallUUID = addTimer(&(*iter3),RTT);
								this->sendPacket("IPv4",this->clonePacket(iter3->recentpacket.this_packet));
								this->freePacket(packet);
								return;
							}
							else{
								this->freePacket(packet);
								return;
							}
						}
						break;
					}
					case state::ESTABLISHED:
					{
						peer_info = (struct sockaddr_in *)(&(iter3->peer_addr));
						if(peer_info->sin_port == rcvdata.src_port && *((uint32_t *)src_ip) == peer_info->sin_addr.s_addr){
							//printf("already esta~\n");
							if(rcvdata.flag2 == flag2::ACK){
								if(data_size == 0){
									this->freePacket(packet);
									return;
								}
								if(iter3->rcv_len + data_size > ntohs(iter3->winsize)){
									this->freePacket(packet);
									return;
								}
								if(ntohl(rcvdata.seqnum) == ntohl(iter3->acknum)){
									//printf("%d\n",data_size);
									iter3->acknum = htonl(ntohl(iter3->acknum)+data_size);
									memcpy(iter3->buf + iter3->buf_use, rcvdata.data, data_size);
									iter3->buf_use += data_size;
									iter3->rcv_len += data_size;
									this->freePacket(packet);

									std::list<struct packet_info>::iterator rcvs;
									rcvs = iter3->rcv_packets.begin();
									while(rcvs != iter3->rcv_packets.end() && iter3->acknum == rcvs->num){
										data_size = rcvs->len;
										rcvs->this_packet->readData(54, iter3->buf + iter3->buf_use, data_size);
										iter3->acknum = htonl(ntohl(iter3->acknum)+data_size);
										iter3->buf_use += data_size;
										this->freePacket(rcvs->this_packet);
										++rcvs;
										iter3->rcv_packets.pop_front();
									}

									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);
									
									return;
								}
								else if(ntohl(rcvdata.seqnum) > ntohl(iter3->acknum)){
									struct packet_info new_rcv;
									new_rcv.num = rcvdata.seqnum;
									new_rcv.len = data_size;
									new_rcv.this_packet = packet; 

									std::list<struct packet_info>::iterator rcvs;
									rcvs = iter3->rcv_packets.begin();
									while(true){
										if(rcvs->num == new_rcv.num){
											this->freePacket(packet);
											return;
										}
										if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
											iter3->rcv_packets.insert(rcvs, new_rcv);
											break;
										}
										++rcvs;
										if(rcvs == iter3->rcv_packets.end()){
											iter3->rcv_packets.push_back(new_rcv);
											break;
										}
									}

									iter3->rcv_len += data_size;

									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);

									return;
								}
								else{
									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);
									this->freePacket(packet);
									return;

								}

							}
							else if(rcvdata.flag2 == flag2::FIN){
								//printf("esta rcv fin\n");
								iter3->state = state::CLOSE_WAIT;
								if(rcvdata.seqnum == iter3->acknum){
									iter3->peer_winsize = rcvdata.winsize;
									iter3->acknum = htonl(ntohl(iter3->acknum) + 1);

									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);
									this->freePacket(packet);
									return;
								}
								else{

									struct packet_info new_rcv;
									new_rcv.num = rcvdata.seqnum;
									new_rcv.len = 0;
									new_rcv.this_packet = packet; 


									std::list<struct packet_info>::iterator rcvs;
									rcvs = iter3->rcv_packets.begin();
									while(true){
										if(rcvs->num == new_rcv.num){
											this->freePacket(packet);
											return;
										}
										if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
											iter3->rcv_packets.insert(rcvs, new_rcv);
											break;
										}
										++rcvs;
										if(rcvs == iter3->rcv_packets.end()){
											iter3->rcv_packets.push_back(new_rcv);
											break;
										}
									}

									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);

									return;
								}
							}
							else{
								this->freePacket(packet);
								return;
							}
						}
						break;
					}
					case state::CLOSE_WAIT:
					{
						peer_info = (struct sockaddr_in *)(&(iter3->peer_addr));
						if(peer_info->sin_port == rcvdata.src_port && *((uint32_t *)src_ip) == peer_info->sin_addr.s_addr){
							if(rcvdata.flag2 == flag2::ACK){
								if(data_size == 0){
									this->freePacket(packet);
									return;
								}
								if(iter3->rcv_len + data_size > ntohs(iter3->winsize)){
									this->freePacket(packet);
									return;
								}
								if(rcvdata.seqnum == iter3->acknum){
									//printf("%d\n",data_size);
									iter3->acknum = htonl(ntohl(iter3->acknum)+data_size);
									memcpy(iter3->buf + iter3->buf_use, rcvdata.data, data_size);
									iter3->buf_use += data_size;
									iter3->rcv_len += data_size;
									this->freePacket(packet);

									std::list<struct packet_info>::iterator rcvs;
									rcvs = iter3->rcv_packets.begin();
									while(rcvs != iter3->rcv_packets.end() && iter3->acknum == rcvs->num){
										data_size = rcvs->len;
										rcvs->this_packet->readData(54, iter3->buf + iter3->buf_use, data_size);
										iter3->buf_use += data_size;
										if(data_size==0){ // for FIN
											data_size = 1;
										}
										iter3->acknum = htonl(ntohl(iter3->acknum)+data_size);
										this->freePacket(rcvs->this_packet);
										++rcvs;
										iter3->rcv_packets.pop_front();
									}

									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);
									
									return;
								}
								else if(ntohl(rcvdata.seqnum) > ntohl(iter3->acknum)){

									struct packet_info new_rcv;
									new_rcv.num = rcvdata.seqnum;
									new_rcv.len = data_size;
									new_rcv.this_packet = packet; 

									std::list<struct packet_info>::iterator rcvs;
									rcvs = iter3->rcv_packets.begin();
									while(rcvs != iter3->rcv_packets.end()){
										if(rcvs->num == new_rcv.num){
											this->freePacket(packet);
											return;
										}
										if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
											iter3->rcv_packets.insert(rcvs, new_rcv);
											break;
										}
										rcvs++;
									}
									if(rcvs == iter3->rcv_packets.end()){
										iter3->rcv_packets.push_back(new_rcv);
									}
									iter3->rcv_len += data_size;

									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);

									return;
								}
								else{
									Packet* myPacket = this->allocatePacket(54); //prepare to send

									struct tcpdata data;

									data.src_port = rcvdata.dest_port;
									data.dest_port = rcvdata.src_port;
								    data.seqnum = iter3->seqnum;
								    data.acknum = iter3->acknum;
								    data.flag1 = 0x50;
								    data.flag2 = flag2::ACK;
								    data.winsize = iter3->winsize;
								    data.checksum = 0;
								    data.urg = 0;

									//swap the src and dest
									myPacket->writeData(14+12, dest_ip, 4);
									myPacket->writeData(14+16, src_ip, 4);
									data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
									myPacket->writeData(34, &data, 20);

									//IP module will fill the rest of the IP header,
									//send it to the correct network interface
									this->sendPacket("IPv4", myPacket);
									this->freePacket(packet);
									return;
								}
							}
							else{
								this->freePacket(packet);
								return;
							}
						}
						break;
					}
					case state::LISTEN:
						listening = &(*iter3);
						break;
					}
				}
				//already accept
				if(dest_context->esta < 0){ // there must listen when esta<0
					if(rcvdata.flag2 == flag2::SYN){
						//printf("whyyyyy\n");
						dest_context->esta += 1;

						listening->connect = true;
						listening->state = state::SYN_RCVD;
						listening->peer_winsize = rcvdata.winsize;

						listening->acknum = htonl(ntohl(rcvdata.seqnum)+1);

						if(((struct sockaddr_in *)(&listening->my_addr))->sin_addr.s_addr == INADDR_ANY){
							((struct sockaddr_in *)(&listening->my_addr))->sin_addr.s_addr = *((uint32_t *)dest_ip);
						}
						struct sockaddr_in peer_addr;
						memset(&peer_addr, 0, sizeof(peer_addr));

						peer_addr.sin_family = AF_INET;
						peer_addr.sin_addr.s_addr = *((uint32_t *)src_ip);
						peer_addr.sin_port = rcvdata.src_port;
						listening->peer_addr = *((struct sockaddr *)(&peer_addr));

						Packet* myPacket = this->allocatePacket(54); //prepare to send
						struct tcpdata data;
						data.src_port = rcvdata.dest_port;
						data.dest_port = rcvdata.src_port;
					    data.seqnum = listening->seqnum;
					    data.acknum = listening->acknum;
					    data.flag1 = 0x50;
					    data.flag2 = flag2::SYN|flag2::ACK;
					    data.winsize = listening->winsize;
					    data.checksum = 0;
					    data.urg = 0;


						//swap the src and dest
						myPacket->writeData(14+12, dest_ip, 4);
						myPacket->writeData(14+16, src_ip, 4);
						data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data,20));
						myPacket->writeData(34, &data, 20);
						//IP module will fill the rest of the IP header,
						//send it to the correct network interface

						listening->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
						listening->recentpacket.this_packet = myPacket;
						listening->recentpacket.syscallUUID = addTimer(listening,RTT);


						this->sendPacket("IPv4", this->clonePacket(myPacket));
						this->freePacket(packet);
						listening->seqnum = htonl(ntohl(listening->seqnum)+1);

						return;
					}
					else{
						this->freePacket(packet);
						return;
					}
				}
				//have space (backlog)
				if(dest_context->connecting < dest_context->backlog){
					if(rcvdata.flag2 == flag2::SYN){
						//printf("want connect\n");
						struct context peer_context;

						struct sockaddr_in peer_addr;
						memset(&peer_addr, 0, sizeof(peer_addr));

						peer_addr.sin_family = AF_INET;
						peer_addr.sin_addr.s_addr = *((uint32_t *)&src_ip);
						peer_addr.sin_port = rcvdata.src_port;
						peer_context.peer_addr = *(struct sockaddr *)(&peer_addr);

						peer_context.my_addr = dest_context->my_addr;
						if(((struct sockaddr_in *)(&peer_context.my_addr))->sin_addr.s_addr == INADDR_ANY){
							((struct sockaddr_in *)(&peer_context.my_addr))->sin_addr.s_addr = *((uint32_t *)dest_ip);
						}
						//printf("%x\n",((struct sockaddr_in *)(&peer_context.my_addr))->sin_addr.s_addr);

						peer_context.state = state::SYN_RCVD;
						peer_context.bind = true;
						peer_context.connect = true;
						peer_context.accept = false;

						peer_context.seqnum = 0;
						peer_context.acknum = htonl(ntohl(rcvdata.seqnum)+1);
						peer_context.winsize = htons(0xc800);
						peer_context.peer_winsize = rcvdata.winsize;
						peer_context.send_len = 0;
						peer_context.send_flow = first_flow;
						peer_context.write = false;
						peer_context.read = false;
						peer_context.rcv_len = 0;
						peer_context.buf_use = 0;
						/*int peer_sockfd;
						if((peer_sockfd = SystemCallInterface::createFileDescriptor(pid))<0){
							this->freePacket(packet);
							return;
						}
						peer_context.sockfd = peer_sockfd;*/

						Packet* myPacket = this->allocatePacket(54); //prepare to send
						
						struct tcpdata data;
						data.src_port = rcvdata.dest_port;
						data.dest_port = rcvdata.src_port;
					    data.seqnum = peer_context.seqnum;
					    data.acknum = peer_context.acknum;
					    data.flag1 = 0x50;
					    data.flag2 = flag2::SYN|flag2::ACK;
						data.winsize = peer_context.winsize;
					    data.checksum = 0;
					    data.urg = 0;

						//swap the src and dest
						myPacket->writeData(14+12, dest_ip, 4);
						myPacket->writeData(14+16, src_ip, 4);
						data.checksum = htons(~(NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20)));
						myPacket->writeData(34, &data, 20);
						//IP module will fill the rest of the IP header,
						//send it to the correct network interface

						peer_context.recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
						peer_context.recentpacket.this_packet = myPacket;
						


						this->sendPacket("IPv4", this->clonePacket(myPacket));
						this->freePacket(packet);

						peer_context.seqnum = htonl(ntohl(peer_context.seqnum)+1);
						dest_context->connect_context.push_back(peer_context);

						dest_context->connect_context.back().recentpacket.syscallUUID = addTimer(&dest_context->connect_context.back(), RTT);

						dest_context->connecting +=1;

						return;
					}
					else{
						this->freePacket(packet);
						return;
					}
				}
				else{
					//printf("full....\n");
					Packet* myPacket = this->clonePacket(packet); //prepare to send
					
					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = 0;
				    data.acknum = 0;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::RST;
				    data.winsize = 0;
				    data.checksum = 0;
				    data.urg = 0;


					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface
					this->sendPacket("IPv4", myPacket);
					this->freePacket(packet);
					return;
				}
				break;
			}
			case state::SYN_SENT:
			{
				if(rcvdata.flag2 == (flag2::SYN|flag2::ACK)){
					//printf("connect succese!!\n");
					if(dest_context->seqnum != rcvdata.acknum){
						this->freePacket(packet);
						return;
					}
					dest_context->state = state::ESTABLISHED;
					dest_context->buf = (uint8_t *)malloc(ntohs(dest_context->winsize));
					dest_context->acknum = htonl(ntohl(rcvdata.seqnum) + 1);
					dest_context->peer_winsize = rcvdata.winsize;
					//dest_context->first_pos = dest_context->acknum;
					Packet* myPacket = this->allocatePacket(54); //prepare to send

					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = dest_context->seqnum;
				    data.acknum = dest_context->acknum;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::ACK;
				    data.winsize = dest_context->winsize;
				    data.checksum = 0;
				    data.urg = 0;

					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface

					cancelTimer(dest_context->recentpacket.syscallUUID);
					this->freePacket(dest_context->recentpacket.this_packet);

					dest_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + 10 * RTT;
					dest_context->recentpacket.this_packet = myPacket;
					dest_context->recentpacket.syscallUUID = addTimer(dest_context, 10 * RTT);

					this->sendPacket("IPv4", this->clonePacket(myPacket));
					this->freePacket(packet);
					returnSystemCall(dest_context->syscallUUID, 0);

					return;
				}
				if(rcvdata.flag2 == flag2::SYN){
					//printf("simultaneous connect!!\n");
					dest_context->state = state::SYN_RCVD;
					dest_context->seqnum = 0;
					dest_context->acknum = htonl(ntohl(rcvdata.seqnum) + 1);
					dest_context->peer_winsize = rcvdata.winsize;

					Packet* myPacket = this->allocatePacket(54); //prepare to send

					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = dest_context->seqnum;
				    data.acknum = dest_context->acknum;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::SYN|flag2::ACK;
				    data.winsize = dest_context->winsize;
				    data.checksum = 0;
				    data.urg = 0;

					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface

					cancelTimer(dest_context->recentpacket.syscallUUID);
					this->freePacket(dest_context->recentpacket.this_packet);

					dest_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
					dest_context->recentpacket.syscallUUID = addTimer(dest_context,RTT);
					dest_context->recentpacket.this_packet = myPacket;

					dest_context->seqnum = htonl(ntohl(dest_context->seqnum) + 1);

					this->sendPacket("IPv4", this->clonePacket(myPacket));
					this->freePacket(packet);
					return;
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::SYN_RCVD:
			{
				if(rcvdata.flag2 == flag2::ACK){
					//printf("simultaneous connect succese!!\n");
					if(dest_context->seqnum != rcvdata.acknum){
						this->freePacket(packet);
						return;
					}
					dest_context->state = state::ESTABLISHED;
					dest_context->peer_winsize = rcvdata.winsize;
					dest_context->buf = (uint8_t *)malloc(ntohs(dest_context->winsize));
					//dest_context->first_pos = dest_context->acknum;

					dest_context->recentpacket.pop_time = 0;
					cancelTimer(dest_context->recentpacket.syscallUUID);
					this->freePacket(dest_context->recentpacket.this_packet);

					returnSystemCall(dest_context->syscallUUID, 0);
					this->freePacket(packet);
					return;
				}
				if(rcvdata.flag2 == (flag2::ACK|flag2::SYN)){
					//printf("simultaneous connect succese!!\n");
					if(dest_context->seqnum != rcvdata.acknum){
						this->freePacket(packet);
						return;
					}
					dest_context->state = state::ESTABLISHED;
					dest_context->buf = (uint8_t *)malloc(ntohs(dest_context->winsize));
					dest_context->acknum = htonl(ntohl(rcvdata.seqnum) + 1);
					dest_context->peer_winsize = rcvdata.winsize;
					//dest_context->first_pos = dest_context->acknum;
					Packet* myPacket = this->allocatePacket(54); //prepare to send

					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = dest_context->seqnum;
				    data.acknum = dest_context->acknum;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::ACK;
				    data.winsize = dest_context->winsize;
				    data.checksum = 0;
				    data.urg = 0;

					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface

					cancelTimer(dest_context->recentpacket.syscallUUID);
					this->freePacket(dest_context->recentpacket.this_packet);

					dest_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + 10 * RTT;
					dest_context->recentpacket.this_packet = myPacket;
					dest_context->recentpacket.syscallUUID = addTimer(dest_context, 10 * RTT);

					this->sendPacket("IPv4", this->clonePacket(myPacket));
					this->freePacket(packet);

					returnSystemCall(dest_context->syscallUUID, 0);
					return;
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::ESTABLISHED:
			{
				if(rcvdata.flag2 == flag2::ACK){
					if(data_size == 0){ // for send data
						//printf("i got ack\n");
						
						std::list<struct packet_info>::iterator sends;
						if(rcvdata.seqnum == dest_context->last_rcv){ // fast retrans
							dest_context->duplicate++;
						}
						else{
							dest_context->last_rcv = rcvdata.seqnum;
							dest_context->duplicate = 0;
						}

						sends = dest_context->send_packets.begin();
						while(sends != dest_context->send_packets.end() && ntohl(rcvdata.acknum) > ntohl(sends->num)){
							data_size = sends->len;
							dest_context->send_len -= data_size;
							cancelTimer(sends->syscallUUID);
							this->freePacket(sends->this_packet);
							++sends;
							dest_context->send_packets.pop_front();
						}

						if(dest_context->duplicate == 3){
							if(sends != dest_context->send_packets.end() && sends->num == rcvdata.seqnum){
								if(sends->sended){
									cancelTimer(sends->syscallUUID);
									sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
									sends->syscallUUID = addTimer(dest_context, RTT);
									this->sendPacket("IPv4", this->clonePacket(sends->this_packet));
								}
								else{
									dest_context->send_len += sends->len;
									sends->sended = true;
									sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
									sends->syscallUUID = addTimer(dest_context, RTT);
									this->sendPacket("IPv4", this->clonePacket(sends->this_packet));
								}
							}
						}
						while(sends != dest_context->send_packets.end() && dest_context->send_len + sends->len <= dest_context->send_flow){
							if(!sends->sended){
								dest_context->send_len += sends->len;
								sends->sended = true;
								sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
								sends->syscallUUID = addTimer(dest_context, RTT);
								Packet *newPacket = this->clonePacket(sends->this_packet);
								this->sendPacket("IPv4", newPacket);
							}
							++sends;
						}

						if(dest_context->write && dest_context->send_len + 512 <= ntohs(dest_context->winsize)){ // can't memory more
							dest_context->write = false;
							SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->write_len);
						}

						this->freePacket(packet);
						return;
					}
					else{ // for rcv data
						if(dest_context->rcv_len + data_size > ntohs(dest_context->winsize)){
							this->freePacket(packet);
							return;
						}
						if(rcvdata.seqnum == dest_context->acknum){
							//printf("rcv : %d",rcvdata.seqnum);
							dest_context->acknum = htonl(ntohl(dest_context->acknum)+data_size);
							memcpy(dest_context->buf + dest_context->buf_use, rcvdata.data, data_size);
							dest_context->buf_use += data_size;
							dest_context->rcv_len += data_size;
							this->freePacket(packet);

							std::list<struct packet_info>::iterator rcvs;
							rcvs = dest_context->rcv_packets.begin();
							while(rcvs != dest_context->rcv_packets.end() && dest_context->acknum == rcvs->num){
								data_size = rcvs->len;
								rcvs->this_packet->readData(54, dest_context->buf + dest_context->buf_use, data_size);
								dest_context->acknum = htonl(ntohl(dest_context->acknum)+data_size);
								dest_context->buf_use += data_size;
								this->freePacket(rcvs->this_packet);
								++rcvs;
								dest_context->rcv_packets.pop_front();
							}

							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);

							if(dest_context->read && dest_context->buf_use >= dest_context->read_len){
								memcpy(dest_context->ret_buf, dest_context->buf, dest_context->read_len);
								SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->read_len);
								dest_context->buf_use -= dest_context->read_len;
								dest_context->rcv_len -= dest_context->read_len;
								//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->read_len);
								uint8_t tmp_buf[dest_context->buf_use];
								memcpy(tmp_buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
								memcpy(dest_context->buf, tmp_buf, dest_context->buf_use);
								dest_context->read = false;
								return;
							}
							
							return;
						}
						else if(ntohl(rcvdata.seqnum) > ntohl(dest_context->acknum)){
							//printf("rcv : %d",rcvdata.seqnum);
							struct packet_info new_rcv;
							new_rcv.num = rcvdata.seqnum;
							new_rcv.len = data_size;
							new_rcv.this_packet = packet; 

							std::list<struct packet_info>::iterator rcvs;
							rcvs = dest_context->rcv_packets.begin();
							while(rcvs != dest_context->rcv_packets.end()){
								if(rcvs->num == new_rcv.num){
									this->freePacket(packet);
									return;
								}
								if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
									dest_context->rcv_packets.insert(rcvs, new_rcv);
									break;
								}
								rcvs++;
							}
							if(rcvs == dest_context->rcv_packets.end()){
								dest_context->rcv_packets.push_back(new_rcv);
							}
							dest_context->rcv_len += data_size;

							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);

							return;
						}
						else{
							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);
							this->freePacket(packet);
							return;
						}
					}
				}
				else if(rcvdata.flag2 == (flag2::SYN|flag2::ACK)){
					if(dest_context->recentpacket.pop_time != 0){
						this->sendPacket("IPv4", this->clonePacket(dest_context->recentpacket.this_packet));
						dest_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + 10 * RTT;
						cancelTimer(dest_context->recentpacket.syscallUUID);
						dest_context->recentpacket.syscallUUID = addTimer(dest_context, 10 * RTT);
						this->freePacket(packet);
						return;
					}
				}
				else if(rcvdata.flag2 == flag2::FIN){
					//printf("esta rcv fin\n");
					dest_context->state = state::CLOSE_WAIT;
					if(rcvdata.seqnum == dest_context->acknum){
						dest_context->acknum = htonl(ntohl(dest_context->acknum) + 1);
						dest_context->peer_winsize = rcvdata.winsize;
						Packet* myPacket = this->allocatePacket(54); //prepare to send

						struct tcpdata data;

						data.src_port = rcvdata.dest_port;
						data.dest_port = rcvdata.src_port;
					    data.seqnum = dest_context->seqnum;
					    data.acknum = dest_context->acknum;
					    data.flag1 = 0x50;
					    data.flag2 = flag2::ACK;
					    data.winsize = dest_context->winsize;
					    data.checksum = 0;
					    data.urg = 0;

						//swap the src and dest
						myPacket->writeData(14+12, dest_ip, 4);
						myPacket->writeData(14+16, src_ip, 4);
						data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
						myPacket->writeData(34, &data, 20);

						//IP module will fill the rest of the IP header,
						//send it to the correct network interface
						this->sendPacket("IPv4", myPacket);
						this->freePacket(packet);

						if(dest_context->read){ // now no more data so read to make true
							if(dest_context->buf_use >= dest_context->read_len){
								memcpy(dest_context->ret_buf, dest_context->buf, dest_context->read_len);
								SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->read_len);
								dest_context->buf_use -= dest_context->read_len;
								dest_context->rcv_len -= dest_context->read_len;
								//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->read_len);
								uint8_t tmp_buf[dest_context->buf_use];
								memcpy(tmp_buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
								memcpy(dest_context->buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
								return;
							}
							if(dest_context->buf_use != 0){
								memcpy(dest_context->ret_buf, dest_context->buf, dest_context->buf_use);
								SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->buf_use);
								dest_context->rcv_len -= dest_context->buf_use;
								dest_context->buf_use = 0;
								return;
							}
							SystemCallInterface::returnSystemCall(dest_context->syscallUUID, -1);
						}
						return;
					}
					else{

						struct packet_info new_rcv;
						new_rcv.num = rcvdata.seqnum;
						new_rcv.len = data_size;
						new_rcv.this_packet = packet; 

						std::list<struct packet_info>::iterator rcvs;
						rcvs = dest_context->rcv_packets.begin();
						while(rcvs != dest_context->rcv_packets.end()){
							if(rcvs->num == new_rcv.num){
								this->freePacket(packet);
								return;
							}
							if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
								dest_context->rcv_packets.insert(rcvs, new_rcv);
								break;
							}
							++rcvs;
						}
						if(rcvs == dest_context->rcv_packets.end()){
							dest_context->rcv_packets.push_back(new_rcv);
						}

						Packet* myPacket = this->allocatePacket(54); //prepare to send

						struct tcpdata data;

						data.src_port = rcvdata.dest_port;
						data.dest_port = rcvdata.src_port;
					    data.seqnum = dest_context->seqnum;
					    data.acknum = dest_context->acknum;
					    data.flag1 = 0x50;
					    data.flag2 = flag2::ACK;
					    data.winsize = dest_context->winsize;
					    data.checksum = 0;
					    data.urg = 0;

						//swap the src and dest
						myPacket->writeData(14+12, dest_ip, 4);
						myPacket->writeData(14+16, src_ip, 4);
						data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
						myPacket->writeData(34, &data, 20);

						//IP module will fill the rest of the IP header,
						//send it to the correct network interface
						this->sendPacket("IPv4", myPacket);

						return;
					}
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::CLOSE_WAIT:
			{
				if(rcvdata.flag2 == flag2::ACK){
					if(data_size == 0){ // for send data
						std::list<struct packet_info>::iterator sends;
						if(rcvdata.seqnum == dest_context->last_rcv){ // fast retrans
							dest_context->duplicate++;
						}
						else{
							dest_context->last_rcv = rcvdata.seqnum;
							dest_context->duplicate = 0;
						}

						sends = dest_context->send_packets.begin();
						while(sends != dest_context->send_packets.end() && ntohl(rcvdata.acknum) > ntohl(sends->num)){
							data_size = sends->len;
							dest_context->send_len -= data_size;
							cancelTimer(sends->syscallUUID);
							this->freePacket(sends->this_packet);
							++sends;
							dest_context->send_packets.pop_front();
						}

						if(dest_context->duplicate == 3){
							if(sends != dest_context->send_packets.end() && sends->num == rcvdata.seqnum){
								if(sends->sended){
									cancelTimer(sends->syscallUUID);
									sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
									sends->syscallUUID = addTimer(dest_context, RTT);
									this->sendPacket("IPv4", this->clonePacket(sends->this_packet));
								}
								else{
									dest_context->send_len += sends->len;
									sends->sended = true;
									sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
									sends->syscallUUID = addTimer(dest_context, RTT);
									this->sendPacket("IPv4", this->clonePacket(sends->this_packet));
								}
							}
						}
						while(sends != dest_context->send_packets.end() && dest_context->send_len + sends->len <= dest_context->send_flow){
							if(!sends->sended){
								dest_context->send_len += sends->len;
								sends->sended = true;
								sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
								sends->syscallUUID = addTimer(dest_context, RTT);
								Packet *newPacket = this->clonePacket(sends->this_packet);
								this->sendPacket("IPv4", newPacket);
							}
							++sends;
						}

						if(dest_context->write && dest_context->send_len + 512 <= ntohs(dest_context->winsize)){ // can't memory more
							dest_context->write = false;
							SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->write_len);
						}

						this->freePacket(packet);
						return;
					}
					else{ // for rcv data
						if(dest_context->rcv_len + data_size > ntohs(dest_context->winsize)){
							this->freePacket(packet);
							return;
						}
						if(rcvdata.seqnum == dest_context->acknum){
							dest_context->acknum = htonl(ntohl(dest_context->acknum)+data_size);
							memcpy(dest_context->buf + dest_context->buf_use, rcvdata.data, data_size);
							dest_context->buf_use += data_size;
							dest_context->rcv_len += data_size;
							this->freePacket(packet);

							std::list<struct packet_info>::iterator rcvs;
							rcvs = dest_context->rcv_packets.begin();
							while(rcvs != dest_context->rcv_packets.end() && dest_context->acknum == rcvs->num){
								data_size = rcvs->len;
								rcvs->this_packet->readData(54, dest_context->buf + dest_context->buf_use, data_size);
								dest_context->buf_use += data_size;
								if(data_size == 0){ // FIN
									dest_context->acknum = htonl(ntohl(dest_context->acknum)+1);
								}
								else{
									dest_context->acknum = htonl(ntohl(dest_context->acknum)+data_size);
								}
								
								this->freePacket(rcvs->this_packet);
								++rcvs;
								dest_context->rcv_packets.pop_front();
							}

							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);

							if(dest_context->read){
								
								if(data_size == 0){
									dest_context->read = true;
									if(dest_context->buf_use >= dest_context->read_len){
										memcpy(dest_context->ret_buf, dest_context->buf, dest_context->read_len);
										SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->read_len);
										dest_context->buf_use -= dest_context->read_len;
										dest_context->rcv_len -= dest_context->read_len;
										//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->read_len);
										uint8_t tmp_buf[dest_context->buf_use];
										memcpy(tmp_buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
										memcpy(dest_context->buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
									}
									if(dest_context->buf_use != 0){
										memcpy(dest_context->ret_buf, dest_context->buf, dest_context->buf_use);
										SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->buf_use);
										dest_context->rcv_len -= dest_context->buf_use;
										//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->buf_use);
										dest_context->buf_use = 0;
									}
									else{
										SystemCallInterface::returnSystemCall(dest_context->syscallUUID, -1);
									}
								}
								else if(dest_context->buf_use >= dest_context->read_len){
									memcpy(dest_context->ret_buf, dest_context->buf, dest_context->read_len);
									SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->read_len);
									dest_context->buf_use -= dest_context->read_len;
									dest_context->rcv_len -= dest_context->read_len;
									//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->read_len);
									uint8_t tmp_buf[dest_context->buf_use];
									memcpy(tmp_buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
									memcpy(dest_context->buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
									dest_context->read = false;
								}
							}
							
							return;
						}
						else if(ntohl(rcvdata.seqnum) > ntohl(dest_context->acknum)){

							struct packet_info new_rcv;
							new_rcv.num = rcvdata.seqnum;
							new_rcv.len = data_size;
							new_rcv.this_packet = packet; 

							std::list<struct packet_info>::iterator rcvs;
							rcvs = dest_context->rcv_packets.begin();
							while(rcvs != dest_context->rcv_packets.end()){
								if(rcvs->num == new_rcv.num){
									this->freePacket(packet);
									return;
								}
								if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
									dest_context->rcv_packets.insert(rcvs, new_rcv);
									break;
								}
								++rcvs;
							}
							if(rcvs == dest_context->rcv_packets.end()){
								dest_context->rcv_packets.push_back(new_rcv);
							}
							dest_context->rcv_len += data_size;

							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);

							return;
						}
						else{
							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);
							this->freePacket(packet);
							return;
						}
					}
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::FIN_WAIT_1:
			{
				if(rcvdata.flag2 == flag2::FIN){
					//printf("fw1 rcv fin\n");
					dest_context->state = state::CLOSING;

					free(dest_context->buf);
					dest_context->acknum = htonl(ntohl(rcvdata.seqnum) + 1);
					dest_context->peer_winsize = rcvdata.winsize;
					Packet* myPacket = this->allocatePacket(54); //prepare to send

					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = dest_context->seqnum;
				    data.acknum = dest_context->acknum;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::ACK;
				    data.winsize = dest_context->winsize;
				    data.checksum = 0;
				    data.urg = 0;

					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface
					this->sendPacket("IPv4", myPacket);
					this->freePacket(packet);
					return;
				}
				else if(rcvdata.flag2 == flag2::ACK){
					if(data_size == 0){ // for send data

						if(rcvdata.acknum == dest_context->seqnum){
							dest_context->send_packets.clear();
							dest_context->state = state::FIN_WAIT_2;
							free(dest_context->buf);
							this->freePacket(packet);
							return;
						}
						else{
							std::list<struct packet_info>::iterator sends;
							if(rcvdata.seqnum == dest_context->last_rcv){ // fast retrans
								dest_context->duplicate++;
							}
							else{
								dest_context->last_rcv = rcvdata.seqnum;
								dest_context->duplicate = 0;
							}

							sends = dest_context->send_packets.begin();
							while(sends != dest_context->send_packets.end() && ntohl(rcvdata.acknum) > ntohl(sends->num)){
								data_size = sends->len;
								dest_context->send_len -= data_size;
								cancelTimer(sends->syscallUUID);
								this->freePacket(sends->this_packet);
								++sends;
								dest_context->send_packets.pop_front();
							}

							if(dest_context->duplicate == 3){
								if(sends != dest_context->send_packets.end() && sends->num == rcvdata.seqnum){
									if(sends->sended){
										cancelTimer(sends->syscallUUID);
										sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
										sends->syscallUUID = addTimer(dest_context, RTT);
										this->sendPacket("IPv4", this->clonePacket(sends->this_packet));
									}
									else{
										dest_context->send_len += sends->len;
										sends->sended = true;
										sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
										sends->syscallUUID = addTimer(dest_context, RTT);
										this->sendPacket("IPv4", this->clonePacket(sends->this_packet));
									}
								}
							}
							while(sends != dest_context->send_packets.end() && dest_context->send_len + sends->len <= dest_context->send_flow){
								if(!sends->sended){
									dest_context->send_len += sends->len;
									sends->sended = true;
									sends->pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
									sends->syscallUUID = addTimer(dest_context, RTT);
									Packet *newPacket = this->clonePacket(sends->this_packet);
									this->sendPacket("IPv4", newPacket);
								}
								++sends;
							}

							if(dest_context->write && dest_context->send_len + 512 <= ntohs(dest_context->winsize)){ // can't memory more
								dest_context->write = false;
								SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->write_len);
							}

							this->freePacket(packet);
							return;
						}

						
					}
					else{ // for rcv data
						if(dest_context->rcv_len + data_size > ntohs(dest_context->winsize)){
							this->freePacket(packet);
							return;
						}
						if(rcvdata.seqnum == dest_context->acknum){
							dest_context->acknum = htonl(ntohl(dest_context->acknum)+data_size);
							memcpy(dest_context->buf + dest_context->buf_use, rcvdata.data, data_size);
							dest_context->buf_use += data_size;
							dest_context->rcv_len += data_size;
							this->freePacket(packet);

							std::list<struct packet_info>::iterator rcvs;
							rcvs = dest_context->rcv_packets.begin();
							while(rcvs != dest_context->rcv_packets.end() && dest_context->acknum == rcvs->num){
								data_size = rcvs->len;
								rcvs->this_packet->readData(54, dest_context->buf + dest_context->buf_use, data_size);
								dest_context->buf_use += data_size;
								if(data_size == 0){ // FIN
									dest_context->acknum = htonl(ntohl(dest_context->acknum)+1);
								}
								else{
									dest_context->acknum = htonl(ntohl(dest_context->acknum)+data_size);
								}
								this->freePacket(rcvs->this_packet);
								++rcvs;
								dest_context->rcv_packets.pop_front();
							}

							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);

							if(dest_context->read){
								
								if(data_size == 0){
									dest_context->read = true;
									if(dest_context->buf_use >= dest_context->read_len){
										memcpy(dest_context->ret_buf, dest_context->buf, dest_context->read_len);
										SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->read_len);
										dest_context->buf_use -= dest_context->read_len;
										dest_context->rcv_len -= dest_context->read_len;
										//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->read_len);
										uint8_t tmp_buf[dest_context->buf_use];
										memcpy(tmp_buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
										memcpy(dest_context->buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
									}
									if(dest_context->buf_use != 0){
										memcpy(dest_context->ret_buf, dest_context->buf, dest_context->buf_use);
										SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->buf_use);
										dest_context->rcv_len -= dest_context->buf_use;
										//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->buf_use);
										dest_context->buf_use = 0;
									}
									else{
										SystemCallInterface::returnSystemCall(dest_context->syscallUUID, -1);
									}
								}
								else if(dest_context->buf_use >= dest_context->read_len){
									memcpy(dest_context->ret_buf, dest_context->buf, dest_context->read_len);
									SystemCallInterface::returnSystemCall(dest_context->syscallUUID, dest_context->read_len);
									dest_context->buf_use -= dest_context->read_len;
									dest_context->rcv_len -= dest_context->read_len;
									//dest_context->first_pos = htonl(ntohl(dest_context->first_pos)+dest_context->read_len);
									uint8_t tmp_buf[dest_context->buf_use];
									memcpy(tmp_buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
									memcpy(dest_context->buf, (dest_context->buf) + dest_context->read_len, dest_context->buf_use);
									dest_context->read = false;
								}
							}
							
							return;
						}
						else if(ntohl(rcvdata.seqnum) > ntohl(dest_context->acknum)){

							struct packet_info new_rcv;
							new_rcv.num = rcvdata.seqnum;
							new_rcv.len = data_size;
							new_rcv.this_packet = packet; 

							std::list<struct packet_info>::iterator rcvs;
							rcvs = dest_context->rcv_packets.begin();
							while(rcvs != dest_context->rcv_packets.end()){
								if(rcvs->num == new_rcv.num){
									this->freePacket(packet);
									return;
								}
								if(ntohl(rcvs->num) > ntohl(new_rcv.num)){
									dest_context->rcv_packets.insert(rcvs, new_rcv);
									break;
								}
								rcvs++;
							}
							if(rcvs == dest_context->rcv_packets.end()){
								dest_context->rcv_packets.push_back(new_rcv);
							}
							dest_context->rcv_len += data_size;

							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);

							return;
						}
						else{
							Packet* myPacket = this->allocatePacket(54); //prepare to send

							struct tcpdata data;

							data.src_port = rcvdata.dest_port;
							data.dest_port = rcvdata.src_port;
						    data.seqnum = dest_context->seqnum;
						    data.acknum = dest_context->acknum;
						    data.flag1 = 0x50;
						    data.flag2 = flag2::ACK;
						    data.winsize = dest_context->winsize;
						    data.checksum = 0;
						    data.urg = 0;

							//swap the src and dest
							myPacket->writeData(14+12, dest_ip, 4);
							myPacket->writeData(14+16, src_ip, 4);
							data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
							myPacket->writeData(34, &data, 20);

							//IP module will fill the rest of the IP header,
							//send it to the correct network interface
							this->sendPacket("IPv4", myPacket);
							this->freePacket(packet);
							return;
						}
					}
				}
				else if(rcvdata.flag2 == (flag2::SYN|flag2::ACK)){
					if(dest_context->recentpacket.pop_time > (this->getHost()->getSystem()->getCurrentTime())){
						this->sendPacket("IPv4", this->clonePacket(dest_context->recentpacket.this_packet));
						dest_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + 10 * RTT;
						cancelTimer(dest_context->recentpacket.syscallUUID);
						dest_context->recentpacket.syscallUUID = addTimer(dest_context, 10 * RTT);
						this->freePacket(packet);
						return;
					}
				}
				else if(rcvdata.flag2 == (flag2::ACK|flag2::FIN)){
					//printf("fw1 rcv ack fin\n");
					if(dest_context->seqnum != rcvdata.acknum){
						this->freePacket(packet);
						return;
					}
					free(dest_context->buf);
					dest_context->state = state::TIME_WAIT;
					dest_context->acknum = htonl(ntohl(dest_context->acknum) + 1);
					dest_context->peer_winsize = rcvdata.winsize;
					Packet* myPacket = this->allocatePacket(54); //prepare to send

					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = dest_context->seqnum;
				    data.acknum = dest_context->acknum;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::ACK;
				    data.winsize = dest_context->winsize;
				    data.checksum = 0;
				    data.urg = 0;

					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface
					this->sendPacket("IPv4", myPacket);
					this->freePacket(packet);

					dest_context->state = state::CLOSED;
					if(dest_context->bind){
						using_ports.remove(rcvdata.dest_port);
					}

					pid_map[pid].erase(sockfd);
					if(pid_map[pid].empty()){
						pid_map.erase(pid);
					}

					SystemCallInterface::removeFileDescriptor(pid, sockfd);

					return;
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::FIN_WAIT_2:
			{
				if(rcvdata.flag2 == flag2::FIN){
					//printf("fw2 rcv fin\n");
					dest_context->state = state::TIME_WAIT;
					dest_context->acknum = htonl(ntohl(dest_context->acknum) + 1);
					dest_context->peer_winsize = rcvdata.winsize;
					Packet* myPacket = this->allocatePacket(54); //prepare to send

					struct tcpdata data;

					data.src_port = rcvdata.dest_port;
					data.dest_port = rcvdata.src_port;
				    data.seqnum = dest_context->seqnum;
				    data.acknum = dest_context->acknum;
				    data.flag1 = 0x50;
				    data.flag2 = flag2::ACK;
				    data.winsize = dest_context->winsize;
				    data.checksum = 0;
				    data.urg = 0;

					//swap the src and dest
					myPacket->writeData(14+12, dest_ip, 4);
					myPacket->writeData(14+16, src_ip, 4);
					data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)dest_ip,*(uint32_t *)src_ip, (uint8_t*)&data, 20));
					myPacket->writeData(34, &data, 20);

					//IP module will fill the rest of the IP header,
					//send it to the correct network interface
					this->sendPacket("IPv4", myPacket);
					this->freePacket(packet);

					dest_context->state = state::CLOSED;
					if(dest_context->bind){
						using_ports.remove(rcvdata.dest_port);
					}

					pid_map[pid].erase(sockfd);
					if(pid_map[pid].empty()){
						pid_map.erase(pid);
					}

					SystemCallInterface::removeFileDescriptor(pid, sockfd);

					return;
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::CLOSING:
			{
				if(rcvdata.flag2 == flag2::ACK){
					//printf("closing rcv ack\n");
					if(dest_context->seqnum != rcvdata.acknum){
						this->freePacket(packet);
						return;
					}
					dest_context->state = state::TIME_WAIT;
					this->freePacket(packet);

					dest_context->state = state::CLOSED;
					if(dest_context->bind){
						using_ports.remove(rcvdata.dest_port);
					}

					pid_map[pid].erase(sockfd);
					if(pid_map[pid].empty()){
						pid_map.erase(pid);
					}

					SystemCallInterface::removeFileDescriptor(pid, sockfd);

					return;
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			case state::LAST_ACK:
			{
				if(rcvdata.flag2 == flag2::ACK){
					//printf("lastack rcv ack\n");
					if(dest_context->seqnum != rcvdata.acknum){
						this->freePacket(packet);
						return;
					}
					dest_context->state = state::CLOSED;
					if(dest_context->bind){
						using_ports.remove(rcvdata.dest_port);
					}

					pid_map[pid].erase(sockfd);
					if(pid_map[pid].empty()){
						pid_map.erase(pid);
					}

					SystemCallInterface::removeFileDescriptor(pid, sockfd);
					this->freePacket(packet);
					return;
				}
				else{
					this->freePacket(packet);
					return;
				}
			}
			}
		}
		this->freePacket(packet);
	}

	void TCPAssignment::timerCallback(void* payload)
	{
		struct context *my_context = (struct context *)payload;
		switch(my_context->state){
		case state::LISTEN:
		case state::SYN_SENT:
		case state::SYN_RCVD:
		{
			my_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT ;
			my_context->recentpacket.syscallUUID = addTimer(my_context,RTT);
			this->sendPacket("IPv4", this->clonePacket(my_context->recentpacket.this_packet));
			break;
		}
		case state::ESTABLISHED:
		case state::FIN_WAIT_1:
		{
			if(my_context->recentpacket.pop_time != 0){
				my_context->recentpacket.pop_time = 0;
				this->freePacket(my_context->recentpacket.this_packet);
				break;
			}
		}
		case state::CLOSE_WAIT:
		{
			struct packet_info *temp_info;
			std::list<struct packet_info>::iterator sends;
			sends = my_context->send_packets.begin();
			temp_info = &(*sends);
			Time small_time = sends->pop_time;
			sends++;
			for(sends; sends != my_context->send_packets.end(); ++sends){
				if(sends->sended){
					if(small_time > sends->pop_time){
						temp_info = &(*sends);
					}
				}
				else{
					break;
				}
			}
			temp_info->pop_time = this->getHost()->getSystem()->getCurrentTime() + RTT;
			temp_info->syscallUUID = addTimer(my_context, RTT);
			this->sendPacket("IPv4", this->clonePacket(temp_info->this_packet));
		}
		}
	}

	void TCPAssignment::syscall_socket(UUID syscallUUID,int	pid, int family, int type){
		int sockfd;
		if(family != AF_INET || type != SOCK_STREAM){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // wrong family or type
			return;
		}
		if((sockfd = SystemCallInterface::createFileDescriptor(pid))<0){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // creat sockfd error
			return;
		}

		struct context newcontext;
		newcontext.state = state::CLOSED;
		newcontext.bind = false;
		newcontext.connect = false;

		newcontext.seqnum = 0;
		newcontext.acknum = 0;
		newcontext.winsize = htons(0xc800);

		newcontext.send_len = 0;
		newcontext.send_flow = first_flow;
		newcontext.write = false;
		newcontext.read = false;
		newcontext.rcv_len = 0;
		newcontext.buf_use = 0;

		pid_map[pid][sockfd] = newcontext;
		
		SystemCallInterface::returnSystemCall(syscallUUID, sockfd);
		return;
	}

	void TCPAssignment::syscall_close(UUID syscallUUID, int	pid, int sockfd){
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(sockfd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no socket exist
			return;
		}
		struct context *close_context = &pid_map[pid][sockfd];
		struct sockaddr_in* my_info;
		struct sockaddr_in* peer_info;
		if(close_context->bind){
			my_info = (struct sockaddr_in*)(&close_context->my_addr);
		}
		if(close_context->connect){
			peer_info = (struct sockaddr_in*)(&close_context->peer_addr);
		}

		switch(close_context->state){
		case state::CLOSED:
		case state::SYN_SENT:
			if(close_context->bind){
				using_ports.remove(my_info->sin_port);
			}
			pid_map[pid].erase(sockfd);
			if(pid_map[pid].empty()){
				pid_map.erase(pid);
			}

			SystemCallInterface::removeFileDescriptor(pid, sockfd);
			SystemCallInterface::returnSystemCall(syscallUUID, 0);
			return;
		case state::ESTABLISHED:
		{
			
			close_context->state = state::FIN_WAIT_1;
			Packet* myPacket = this->allocatePacket(54); //prepare to send
			struct tcpdata data;
			data.src_port = my_info->sin_port;
			data.dest_port = peer_info->sin_port;
		    data.seqnum = close_context->seqnum;
		    data.acknum = 0;
		    data.flag1 = 0x50;
		    data.flag2 = flag2::FIN;
		    data.winsize = close_context->winsize;
		    data.checksum = 0;
		    data.urg = 0;

			myPacket->writeData(14+12, &my_info->sin_addr.s_addr, 4);
			myPacket->writeData(14+16, &peer_info->sin_addr.s_addr, 4);
			data.checksum = htons(~NetworkUtil::tcp_sum(my_info->sin_addr.s_addr,peer_info->sin_addr.s_addr, (uint8_t*)&data,20));
			myPacket->writeData(34, &data, 20);
			//IP module will fill the rest of the IP header,
			//send it to the correct network interface
			if(close_context->send_packets.empty()){
				this->sendPacket("IPv4", myPacket);
				close_context->seqnum = htonl(ntohl(close_context->seqnum)+1);
				SystemCallInterface::returnSystemCall(syscallUUID, 0);
				return;
			}
			else{
				close_context->seqnum = htonl(ntohl(close_context->seqnum)+1);

				struct packet_info new_send;
				new_send.num = close_context->seqnum; //rcv same ack num, pop
				new_send.sended = false;
				new_send.syscallUUID = 0; // for later retrans
				new_send.len = 0; // for send_win, max_sendflow update;
				new_send.this_packet = myPacket;
				
				close_context->send_packets.push_back(new_send);
				SystemCallInterface::returnSystemCall(syscallUUID, 0);
				return;
			}
			
		}
		case state::SYN_RCVD:
		{
			close_context->state = state::FIN_WAIT_1;
			Packet* myPacket = this->allocatePacket(54); //prepare to send
			struct tcpdata data;
			data.src_port = my_info->sin_port;
			data.dest_port = peer_info->sin_port;
		    data.seqnum = close_context->seqnum;
		    data.acknum = 0;
		    data.flag1 = 0x50;
		    data.flag2 = flag2::FIN;
		    data.winsize = close_context->winsize;
		    data.checksum = 0;
		    data.urg = 0;

			myPacket->writeData(14+12, &my_info->sin_addr.s_addr, 4);
			myPacket->writeData(14+16, &peer_info->sin_addr.s_addr, 4);
			data.checksum = htons(~NetworkUtil::tcp_sum(my_info->sin_addr.s_addr,peer_info->sin_addr.s_addr, (uint8_t*)&data,20));
			myPacket->writeData(34, &data, 20);
			//IP module will fill the rest of the IP header,
			//send it to the correct network interface
			this->sendPacket("IPv4", myPacket);

			close_context->seqnum = htonl(ntohl(close_context->seqnum)+1);
			SystemCallInterface::returnSystemCall(syscallUUID, 0);
			return;
		}
		case state::CLOSE_WAIT:
		{
			close_context->state = state::LAST_ACK;

			free(close_context->buf);

			Packet* myPacket = this->allocatePacket(54); //prepare to send
			struct tcpdata data;
			data.src_port = my_info->sin_port;
			data.dest_port = peer_info->sin_port;
		    data.seqnum = close_context->seqnum;
		    data.acknum = 0;
		    data.flag1 = 0x50;
		    data.flag2 = flag2::FIN;
		    data.winsize = close_context->winsize;
		    data.checksum = 0;
		    data.urg = 0;

			myPacket->writeData(14+12, &my_info->sin_addr.s_addr, 4);
			myPacket->writeData(14+16, &peer_info->sin_addr.s_addr, 4);
			data.checksum = htons(~NetworkUtil::tcp_sum(my_info->sin_addr.s_addr,peer_info->sin_addr.s_addr, (uint8_t*)&data,20));
			myPacket->writeData(34, &data, 20);
			//IP module will fill the rest of the IP header,
			//send it to the correct network interface
			this->sendPacket("IPv4", myPacket);

			close_context->seqnum = htonl(ntohl(close_context->seqnum)+1);
			SystemCallInterface::returnSystemCall(syscallUUID, 0);
			return;
		}
		}
	}

	void TCPAssignment::syscall_read(UUID syscallUUID, int pid, int fd, void *buf, size_t count){
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(fd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no exist error
			return;
		}
		struct context *rcv_context = &pid_map[pid][fd];
		switch(rcv_context->state){
			case state::ESTABLISHED:
			case state::CLOSE_WAIT:
				break;
			default:
				SystemCallInterface::returnSystemCall(syscallUUID, -1);
				return;
		}

		if(rcv_context->read){
			if(rcv_context->buf_use == 0){
				SystemCallInterface::returnSystemCall(syscallUUID, -1);
				return;
			}
			else if(rcv_context->buf_use < count){
				memcpy(buf, rcv_context->buf, rcv_context->buf_use);
				SystemCallInterface::returnSystemCall(syscallUUID, rcv_context->buf_use);
				rcv_context->rcv_len -= rcv_context->buf_use;
				rcv_context->buf_use -= 0;
				return;
			}
			else{
				memcpy(buf, rcv_context->buf, count);
				SystemCallInterface::returnSystemCall(syscallUUID, count);
				rcv_context->buf_use -= count;
				rcv_context->rcv_len -= count;
				//rcv_context->first_pos = htonl(ntohl(rcv_context->first_pos) + count);
				uint8_t tmp_buf[rcv_context->buf_use];
				memcpy(tmp_buf, (rcv_context->buf) + count, rcv_context->buf_use);
				memcpy(rcv_context->buf, tmp_buf, rcv_context->buf_use);
				return;
			}
			
		}
		else if(rcv_context->buf_use < count){
			
			rcv_context->syscallUUID = syscallUUID;
			rcv_context->read = true;
			rcv_context->read_len = count;
			rcv_context->ret_buf = buf;
			return;
		}
		else{
			memcpy(buf, rcv_context->buf, count);
			SystemCallInterface::returnSystemCall(syscallUUID, count);
			rcv_context->buf_use -= count;
			rcv_context->rcv_len -= count;
			//rcv_context->first_pos = htonl(ntohl(rcv_context->first_pos) + count);
			uint8_t tmp_buf[rcv_context->buf_use];
			memcpy(tmp_buf, (rcv_context->buf) + count, rcv_context->buf_use);
			memcpy(rcv_context->buf, tmp_buf, rcv_context->buf_use);
			return;
		}

	}

	void TCPAssignment::syscall_write(UUID syscallUUID, int pid, int fd, void *buf, size_t count){
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(fd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no exist error
			return;
		}
		
		struct context *rcv_context = &pid_map[pid][fd];

		switch(rcv_context->state){
			case state::ESTABLISHED: //or don't need to send data
				break;
			default:
				SystemCallInterface::returnSystemCall(syscallUUID, -1);
				return;
		}

		if(count > 512){
			//printf("writing1\n");

			uint8_t src_ip[4];
			uint8_t dest_ip[4];
			memcpy(src_ip,&((struct sockaddr_in *)(&rcv_context->my_addr))->sin_addr.s_addr, 4);
			memcpy(dest_ip,&((struct sockaddr_in *)(&rcv_context->peer_addr))->sin_addr.s_addr, 4);

			Packet* myPacket = allocatePacket(54 + 512); //prepare to send

			struct tcpdata data;

			data.src_port = ((struct sockaddr_in *)(&rcv_context->my_addr))->sin_port;
			data.dest_port = ((struct sockaddr_in *)(&rcv_context->peer_addr))->sin_port;
		    data.seqnum = rcv_context->seqnum;
		    data.acknum = rcv_context->acknum;
		    data.flag1 = 0x50;
		    data.flag2 = flag2::ACK;
		    data.winsize = rcv_context->winsize;
		    data.checksum = 0;
		    data.urg = 0;
		    memcpy(data.data, buf, 512);
			
			myPacket->writeData(14+12, src_ip, 4);
			myPacket->writeData(14+16, dest_ip, 4);
			data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)src_ip,*(uint32_t *)dest_ip, (uint8_t*)&data, 20 + 512));
			myPacket->writeData(34, &data, 20 + 512);

			

			struct packet_info new_send;
			new_send.num = rcv_context->seqnum; //rcv same ack num, pop
			new_send.sended = false;
			new_send.syscallUUID = 0; // for later retrans
			new_send.len = 512; // for send_win, max_sendflow update;
			new_send.this_packet = myPacket;
			
			rcv_context->seqnum = htonl(ntohl(rcv_context->seqnum)+512);

			if(rcv_context->send_len + 512 <= rcv_context->send_flow){
				rcv_context->send_len += 512;
				new_send.sended = true;
				new_send.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
				new_send.syscallUUID = addTimer(rcv_context, RTT);
				Packet *newPacket = this->clonePacket(myPacket);
				this->sendPacket("IPv4", newPacket);
			}
			rcv_context->send_packets.push_back(new_send);

			if(rcv_context->send_len + 512 + 512 > ntohs(rcv_context->winsize)){ // can't memory more
				rcv_context->write = true;
				rcv_context->write_len = 512;
				rcv_context->syscallUUID = syscallUUID;
				return;
			}

			SystemCallInterface::returnSystemCall(syscallUUID, 512);
			return;
		}
		else{
			//printf("writing2\n");

			uint8_t src_ip[4];
			uint8_t dest_ip[4];
			memcpy(src_ip,&((struct sockaddr_in *)(&rcv_context->my_addr))->sin_addr.s_addr, 4);
			memcpy(dest_ip,&((struct sockaddr_in *)(&rcv_context->peer_addr))->sin_addr.s_addr, 4);

			Packet* myPacket = allocatePacket(54 + count);

			struct tcpdata data;

			data.src_port = ((struct sockaddr_in *)(&rcv_context->my_addr))->sin_port;
			data.dest_port = ((struct sockaddr_in *)(&rcv_context->peer_addr))->sin_port;
		    data.seqnum = rcv_context->seqnum;
		    data.acknum = rcv_context->acknum;
		    data.flag1 = 0x50;
		    data.flag2 = flag2::ACK;
		    data.winsize = rcv_context->winsize;
		    data.checksum = 0;
		    data.urg = 0;
		    memcpy(data.data, buf, count);
			
			myPacket->writeData(14+12, src_ip, 4);
			myPacket->writeData(14+16, dest_ip, 4);
			data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)src_ip,*(uint32_t *)dest_ip, (uint8_t*)&data, 20 + count));
			myPacket->writeData(34, &data, 20 + count);

			

			struct packet_info new_send;
			new_send.num = rcv_context->seqnum; //rcv same ack num, pop
			new_send.sended = false;
			new_send.syscallUUID = 0; // for later retrans
			new_send.len = count; // for send_win, max_sendflow update;
			new_send.this_packet = myPacket;
			
			rcv_context->seqnum = htonl(ntohl(rcv_context->seqnum)+count);

			if(rcv_context->send_len + count <= rcv_context->send_flow){
				rcv_context->send_len += count;
				new_send.sended = true;
				new_send.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
				new_send.syscallUUID = addTimer(rcv_context, RTT);
				Packet *newPacket = this->clonePacket(myPacket);
				this->sendPacket("IPv4", newPacket);
			}
			rcv_context->send_packets.push_back(new_send);

			if(rcv_context->send_len + count + 512 > ntohs(rcv_context->winsize)){ // can't send more
				rcv_context->write = true;
				rcv_context->write_len = count;
				rcv_context->syscallUUID = syscallUUID;
				return;
			}

			SystemCallInterface::returnSystemCall(syscallUUID, count);
			return;
		}

	}

	void TCPAssignment::syscall_connect(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t addrlen){

		//printf("connect\n");
		
		
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(sockfd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no exist error
			return;
		}

		struct context *client_context = &pid_map[pid][sockfd];
		client_context->state = state::SYN_SENT;
	    client_context->peer_addr = *addr;
	    client_context->connect = true;
	    client_context->syscallUUID = syscallUUID;


	    struct sockaddr_in* addr_in = (struct sockaddr_in *)addr;
	    uint8_t src_ip[4];
	    uint16_t src_port;

	    if(!client_context->bind){
	    	
		    src_port = this->getHost()->getRoutingTable((uint8_t *) &(addr_in->sin_addr));
		    this->getHost()->getIPAddr(src_ip,src_port);

		    bool already_using_port = true;
		    src_port = (uint16_t)(rand()%0x10000);
		    while(already_using_port){
		    	already_using_port = false;
			    std::list<uint16_t>::iterator iter;
				for (iter = using_ports.begin(); iter != using_ports.end(); ++iter) {
					if(*iter == src_port){
						already_using_port = true;
						break;
					}
				}
				if(src_port == 0xffff){
					src_port = 0x0;
				}
				else{
					src_port += 1;
				}
			}
			using_ports.push_back(src_port);

		    struct sockaddr_in my_addr;
			memset(&my_addr, 0, sizeof(my_addr));

			my_addr.sin_family = AF_INET;
			my_addr.sin_addr.s_addr = *((uint32_t *)&src_ip);
			my_addr.sin_port = src_port;

			client_context->my_addr = *((struct sockaddr*)&my_addr);
			client_context->bind = true;
		}
		else{
			memcpy(src_ip,&((struct sockaddr_in *)(&client_context->my_addr))->sin_addr.s_addr,4);
			src_port = ((struct sockaddr_in *)(&client_context->my_addr))->sin_port;
		}



	    Packet* myPacket = allocatePacket(54);
	    struct tcpdata data;
	    data.src_port = src_port;
	    data.dest_port = addr_in->sin_port;
	    data.seqnum = client_context->seqnum;
	    data.acknum = client_context->acknum;
	    data.flag1 = 0x50;
	    data.flag2 = flag2::SYN;
	    data.winsize = htons(0xc800);
	    data.checksum = 0;
	    data.urg = 0;

	    //printf("%x, %x\n", *(uint32_t *)src_ip, (addr_in->sin_addr.s_addr));

		myPacket->writeData(14+12, src_ip, 4); //src ip
		myPacket->writeData(14+16, &(addr_in->sin_addr.s_addr), 4); //dst ip

		data.checksum = htons(~NetworkUtil::tcp_sum(*(uint32_t *)src_ip,addr_in->sin_addr.s_addr, (uint8_t*)&data,20));

		myPacket->writeData(34, &data, 20);
		//IP module will fill the rest of the IP header,
		//send it to the correct network interface
		
		client_context->recentpacket.pop_time = (this->getHost()->getSystem()->getCurrentTime()) + RTT;
		client_context->recentpacket.syscallUUID = addTimer(client_context,RTT);
		client_context->recentpacket.this_packet = myPacket;

		this->sendPacket("IPv4", this->clonePacket(myPacket));

		client_context->seqnum = htonl(ntohl(client_context->seqnum)+1);

	}

	void TCPAssignment::syscall_listen(UUID syscallUUID, int pid, int sockfd, int backlog){
		//printf("listen\n");
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(sockfd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no exist error
			return;
		}

		struct context *server_context = &pid_map[pid][sockfd];
		server_context->state = state::LISTEN;
	    server_context->backlog = backlog;
	    server_context->connecting = 0;
	    server_context->esta = 0;

	    SystemCallInterface::returnSystemCall(syscallUUID, 0);
	    return;
	}

	void TCPAssignment::syscall_accept(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t *addrlen){
		//printf("accept\n");
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(sockfd) == pid_map[pid].end()){
			//printf("accept no exist\n");
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no exist error
			return;
		}

		struct context *server_context = &pid_map[pid][sockfd];

		if(server_context->state != state::LISTEN){
			//printf("accept no listen\n");
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // not listen state
			return;
		}

		if(server_context->esta > 0){
			std::list<struct context>::iterator iter;
			for (iter = server_context->connect_context.begin(); iter != server_context->connect_context.end(); ++iter) {
				if(iter->state == state::ESTABLISHED || iter->state == state::CLOSE_WAIT ){
					server_context->esta -= 1;
					int sockfd;
					if((sockfd = SystemCallInterface::createFileDescriptor(pid))<0){
						SystemCallInterface::returnSystemCall(syscallUUID, -1); // creat sockfd error
						return;
					}
					pid_map[pid][sockfd] = *iter;
					*addr = pid_map[pid][sockfd].peer_addr;
					using_ports.push_back(((struct sockaddr_in*)(&pid_map[pid][sockfd].my_addr))->sin_port);
					server_context->connect_context.erase(iter);
					SystemCallInterface::returnSystemCall(syscallUUID, sockfd);
					return;
				}
			}
		}

		if(server_context->connecting > 0){
			std::list<struct context>::iterator iter;
			for (iter = server_context->connect_context.begin(); iter != server_context->connect_context.end(); ++iter) {
				if(iter->state == state::SYN_RCVD && iter->accept == false){
					iter->accept = true;
					iter->acceptaddr = addr;
					iter->syscallUUID = syscallUUID;
					return;
				}
			}
		}

		if(server_context->connecting < server_context->backlog){ // may always true

			server_context->esta -= 1;
			server_context->connecting += 1;
			struct context wait_context;
			wait_context.my_addr = server_context->my_addr;
			wait_context.state = state::LISTEN;
			wait_context.bind = true;
			wait_context.connect = false;
			wait_context.syscallUUID = syscallUUID;
			wait_context.accept = true;
			wait_context.acceptaddr = addr; //save for after return
			wait_context.seqnum = 0;
			wait_context.acknum = 0;
			wait_context.winsize = htons(0xc800);
			wait_context.send_len = 0;
			wait_context.send_flow = first_flow;
			wait_context.write = false;
			wait_context.read = false;
			wait_context.rcv_len = 0;
			wait_context.buf_use = 0;
			
			/*int wait_sockfd;
			if((wait_sockfd = SystemCallInterface::createFileDescriptor(pid))<0){
				SystemCallInterface::returnSystemCall(syscallUUID, -1); // creat sockfd error
				return;
			}
			wait_context.sockfd = wait_sockfd;*/

			server_context->connect_context.push_back(wait_context);
		}

	}

	void TCPAssignment::syscall_bind(UUID syscallUUID, int pid, int sockfd,
					struct sockaddr *my_addr,
					int addrlen){
		if(pid_map.find(pid) != pid_map.end()){
			if(pid_map[pid].find(sockfd) != pid_map[pid].end()){
				if(pid_map[pid][sockfd].bind){
					SystemCallInterface::returnSystemCall(syscallUUID, -1); // already exist bind
					return;
				}
			}
		}

		std::map<int, std::map<int, struct context>>::iterator iter;
		std::map<int, struct context>::iterator iter2;
		for (iter = pid_map.begin(); iter != pid_map.end(); ++iter) {
			for (iter2 = (iter->second.begin()); iter2 != iter->second.end(); ++iter2){
				struct sockaddr_in info = *((struct sockaddr_in *)(&(iter2->second.my_addr)));
				struct sockaddr_in new_info = *((struct sockaddr_in *)my_addr);
				if(info.sin_port == new_info.sin_port){
					if(info.sin_addr.s_addr == INADDR_ANY || new_info.sin_addr.s_addr == INADDR_ANY){
						SystemCallInterface::returnSystemCall(syscallUUID, -1);
						return;
					}
					else if(info.sin_addr.s_addr == new_info.sin_addr.s_addr){
						SystemCallInterface::returnSystemCall(syscallUUID, -1);
						return;
					}
				}
			}
		}

		pid_map[pid][sockfd].bind = true;
		pid_map[pid][sockfd].my_addr = *my_addr;
		using_ports.push_back(((struct sockaddr_in*)my_addr)->sin_port);

		SystemCallInterface::returnSystemCall(syscallUUID, 0);
		return;
	}
	
	void TCPAssignment::syscall_getsockname(UUID syscallUUID, int pid, int sockfd,
					struct sockaddr *addr,
					socklen_t *addrlen){
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(sockfd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no bind exist error
			return;
		}
		if(!pid_map[pid][sockfd].bind){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no bind exist error
			return;
		}

		//memcpy(addr,&(pid_map[pid][sockfd]),sizeof(struct sockaddr));
		*addr = pid_map[pid][sockfd].my_addr;
		*addrlen = sizeof(struct sockaddr);

		SystemCallInterface::returnSystemCall(syscallUUID, 0);
		return;
	}

	void TCPAssignment::syscall_getpeername(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t *addrlen){
		if(pid_map.find(pid) == pid_map.end() || pid_map[pid].find(sockfd) == pid_map[pid].end()){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no peer exist error
			return;
		}
		if(!pid_map[pid][sockfd].connect){
			SystemCallInterface::returnSystemCall(syscallUUID, -1); // no peer exist error
			return;
		}

		//memcpy(addr,&(pid_map[pid][sockfd]),sizeof(struct sockaddr));
		*addr = pid_map[pid][sockfd].peer_addr;
		*addrlen = sizeof(struct sockaddr);

		SystemCallInterface::returnSystemCall(syscallUUID, 0);
		return;
	}
}