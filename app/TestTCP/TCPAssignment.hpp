/*
 * E_TCPAssignment.hpp
 *
 *  Created on: 2014. 11. 20.
 *      Author: 근홍
 */

#ifndef E_TCPASSIGNMENT_HPP_
#define E_TCPASSIGNMENT_HPP_


#include <E/Networking/E_Networking.hpp>
#include <E/Networking/E_Host.hpp>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <map>
#include <list>


#include <E/E_TimerModule.hpp>

#define first_flow (512*50)
#define RTT (100000000)

namespace state{
	enum Enum{
		CLOSED,
		LISTEN,
		SYN_SENT,
		SYN_RCVD,
		ESTABLISHED,
		FIN_WAIT_1,
		FIN_WAIT_2,
		CLOSING,
		TIME_WAIT,
		CLOSE_WAIT,
		LAST_ACK
	};
}

namespace flag2{
	enum Enum{
		FIN = 0x1,
		SYN = 0x2,
		RST = 0x4,
		PSH = 0x8,
		ACK = 0x10,
		URG = 0x20,
		ECE = 0x40,
		CWR = 0x80
	};
}

namespace E
{

	class TCPAssignment : public HostModule, public NetworkModule, public SystemCallInterface, private NetworkLog, private TimerModule
	{
		private:
			virtual void timerCallback(void* payload) final;
			/*struct sockaddr {
			    unsigned short    sa_family;    // address family, AF_xxx
			    char              sa_data[14];  // 14 bytes of protocol address
			}; */

			/*struct in_addr {
			    uint32_t s_addr; // that's a 32-bit int (4 bytes)
			};

			struct sockaddr_in {
			    short int          sin_family;  // Address family, AF_INET
			    unsigned short int sin_port;    // Port number
			    struct in_addr     sin_addr;    // Internet address
			    unsigned char      sin_zero[8]; // Same size as struct sockaddr
			};*/

			/*struct in6_addr {
			    unsigned char s6_addr[16];   // IPv6 address
			};

			struct sockaddr_in6 {
			    uint16_t       sin6_family;   // address family, AF_INET6
			    uint16_t       sin6_port;     // port number, Network Byte Order
			    uint32_t       sin6_flowinfo; // IPv6 flow information
			    struct in6_addr sin6_addr;     // IPv6 address
			    uint32_t       sin6_scope_id; // Scope ID
			};*/

		public:
			TCPAssignment(Host* host);
			virtual void initialize();
			virtual void finalize();
			virtual ~TCPAssignment();
			virtual void syscall_socket(UUID syscallUUID,int pid, int domain, int protocol);
			virtual void syscall_close(UUID syscallUUID, int pid, int sockfd);
			virtual void syscall_read(UUID syscallUUID, int pid, int fd, void *buf, size_t count);
			virtual void syscall_write(UUID syscallUUID, int pid, int fd, void *buf, size_t count);
			virtual void syscall_connect(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t addrlen);
			virtual void syscall_listen(UUID syscallUUID, int pid, int sockfd, int backlog);
			virtual void syscall_accept(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t *addrlen);
			virtual void syscall_bind(UUID syscallUUID, int pid, int sockfd, struct sockaddr *my_addr, int addrlen);
			virtual void syscall_getsockname(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t *addrlen);
			virtual void syscall_getpeername(UUID syscallUUID, int pid, int sockfd, struct sockaddr *addr, socklen_t *addrlen);


			struct packet_info{
				uint32_t num;
				bool sended;
				Time pop_time;
				UUID syscallUUID;
				uint16_t len;
				Packet* this_packet;
			};
			struct context {
				struct sockaddr my_addr;
				struct sockaddr peer_addr;
				int state;
				//int sockfd;
				bool bind;
				bool connect;
				UUID syscallUUID;
				//bool is_bound;
				//struct context *server;
				int backlog;
				int connecting;
				int esta;
				bool accept;
				struct sockaddr *acceptaddr; // for update
				std::list<struct context> connect_context;
				struct packet_info recentpacket;

				uint32_t seqnum;
			    uint32_t acknum;
			    uint16_t winsize;
			    uint16_t peer_winsize;

			    uint16_t send_len; // send packet data len
			    uint16_t send_flow; // for sendflow
			    std::list<struct packet_info> send_packets;
			    bool write;
			    uint16_t write_len;
			    Time pop_load;
			    uint32_t last_rcv;
			    int duplicate;

			    bool read;
			    int read_len;
			    //uint32_t first_pos;
			    uint16_t rcv_len;
			    uint8_t *buf; //create when esta, delete when close
			    void *ret_buf;
			    uint16_t buf_use;
			    std::list<struct packet_info> rcv_packets;
			};
			struct tcpdata{
				uint16_t src_port;
			    uint16_t dest_port;
			    uint32_t seqnum;
			    uint32_t acknum;
			    uint8_t flag1;
			    uint8_t flag2;
			    uint16_t winsize;
			    uint16_t checksum;
			    uint16_t urg;
			    uint8_t data[512]; 
			};
			
			std::map<int, std::map<int, struct context>> pid_map; // (pid, (sockfd, context)) info
			std::list<uint16_t> using_ports;

		protected:
			virtual void systemCallback(UUID syscallUUID, int pid, const SystemCallParameter& param) final;
			virtual void packetArrived(std::string fromModule, Packet* packet) final;
	};

	class TCPAssignmentProvider
	{
		private:
			TCPAssignmentProvider() {}
			~TCPAssignmentProvider() {}
		public:
			static HostModule* allocate(Host* host) { return new TCPAssignment(host); }
	};

}


#endif /* E_TCPASSIGNMENT_HPP_ */
